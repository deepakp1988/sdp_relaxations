	import numpy as np
	from ncpol2sdpa import *


	s = np.array([1, 1, -1, -1])
	c = -np.array([-1,-2,-3,-4,-5,-6])
	l = np.array([1,1,1,1,1,1])
	b = np.array([1,1,1,1,1,1])
	k=1
	ini_condtn = np.array([0.1,0.2,0.4,0.5,0.8,1])


	n_vars = 13 # Number of variables
	x = generate_variables('x', n_vars)


	obj = - x[0]

	inequalities = [-x[0]*x[1]*x[3]*x[5]*x[7]*x[9]*x[11]+x[2]*x[4]*x[6]*x[8]*x[10]*x[12]**(k+1)]
	for i in range(n_vars-2):
	    inequalities.append(x[i+2]-x[i+1])

	inequalities.append(x[1]-1)
	    
	equalities = []
	for i in range(len(ini_condtn)):
	    equalities.append(ini_condtn[i]*c[i]/(b[i]*l[i])-x[1]**c[i]+x[2]**c[i]-s[0]*x[3]**c[i]+s[0]*x[4]**c[i]-s[1]*x[5]**c[i]+s[1]*x[6]**c[i]-s[2]*x[7]**c[i]+s[2]*x[8]**c[i]-s[3]*x[9]**c[i]+s[3]*x[10]**c[i]+x[11]**c[i]-x[12]**c[i])



level = 4  # Requested level of relaxation
sdp = SdpRelaxation(x)
sdp.get_relaxation(level, objective=obj, inequalities=inequalities, equalities=equalities, chordal_extension=True)

sdp.solve()
print(sdp.primal, sdp.dual, sdp.status)
